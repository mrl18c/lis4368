> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 Advanced Web App Development

## Matthew Lewis

### Project #2 Requirements:

5 parts:

1. Client and server side form validation
2. Collect customer information and store in mysql database
3. Provide customer data on web servlet
4. Provide Bitbucket Read-only access to repo
5. Screenshots: Valid user entry, successful validation, display data, modify form, modified data, delete warning, updated mysql tables


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>




#### Assignment Screenshot and Links:)
*Screenshot of valid user entry*:
![Valid user entry](img/passedValidation.png "Valid user entry based upon P2 requirements")

*Screenshot of successful validation:*
![Successful validation](img/thanksPage.png "Successful validation based upon P2 requirements")

*Screenshot of data on display:*
![Display data](img/TableInsert.png "Showing customer data based upon P2 requirements")

*Screenshot of modify form:*
![Modify form](img/ModifiedData.png "Showing modify customer form based upon P2 requirements")

*Screenshot of modified data:*
![Modified data](img/TableUpdated.png "Showing modified data based upon P2 requirements")

*Screenshot of delete warning:*
![Delete warning](img/ConfirmDelete.png "Showing delete warning based upon P2 requirements")

*Screenshot of associated tables (select):*
![Customer select](img/mysqlSelect.png "Showing associated table based upon P2 requirements")

*Screenshot of associated tables (insert):*
![Customer insert](img/mysqlInsert.png "Showing associated table based upon P2 requirements")

*Screenshot of associated tables (update):*
![Customer update](img/mysqlUpdate.png "Showing associated table based upon P2 requirements")

*Screenshot of associated tables (delete):*
![Customer delete](img/mysqlDelete.png "Showing associated table based upon P2 requirements")

