> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 Advanced Web App Development

## Matthew Lewis

### Assignment #2 Requirements:

3 parts:

1. Install MySQL Workbench
2. Connect MySQL database to Java Servlet
3. Chapter questions (CH. 5&6)

#### README.md file should include the following items:

* Screenshot of running https://localhost:9999/hello
* Screenshot of running http://localhost:9999/hello/sayhello
* Screenshot of running http://localhost:9999/hello/querybook.html



#### Assignment Screenshots:

*Screenshot of running http://localhost:9999/hello

![Hello](img/hello.png "hello")

*Screenshot of running https://localhost:9999/hello/sayhello

![Say Hello](img/sayhello.png "sayhello")

*Screenshot of running https://localhost:9999/hello/querybook.html

![Querybook](img/queerybook.png "querybook")

*Screenshot of of database query

![Query](img/records.png "query")






