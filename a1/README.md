> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 Advanced Web App Development

## Matthew Lewis

### Assignment #1 Requirements:

3 parts:

1. Distributed version control with Git and Bitbucket
2. Java/JSP/Servlet Development installations
3. Chapter questions (CH. 1-4)

#### README.md file should include the following items:

* Screenshot of runing java Hello
* Screenshot of running http://localhost:9999
* Git commands with short descriptions
* Bitbucket repo links: a)This assignment completed and b)the completed tutorial above (bitbucketstationlocations)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - create a new local git repository 
2. git status - list the files you've changed and those you till need to add or commit
3. git add - add one or more files to staging (index)
4. git commit - commit changes to head (but not yet to the remote repo)
5. git push - send changes to the master branch of your remote repo
6. git pull - fetch and merge changes on the remote server to your working directory
7. git checkout -- <filename> - if you mess up, you can replace the changes in your working tree with the last content in head: changes already added to the index as well as new files will be kept

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](C:\Users\Mattl\repos\lis4368\a1\img\Screenshot(31).png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](C:\Users\Mattl\repos\lis4368\a1\img\Screenshot(33).png)



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/mrl18c/bitbucketstationlocations/ "Bitbucket Station Locations")


