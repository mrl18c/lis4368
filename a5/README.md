> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 Advanced Web App Development

## Matthew Lewis

### Assignment #5 Requirements:

4 parts:

1. Server side form validation
2. Collect customer information and store in mysql database
3. Provide Bitbucket Read-only access to repo
4. Screenshots: Failed validation, successful validation, updated mysql table


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>




#### Assignment Screenshot and Links:)
*Screenshot of failed validation*:
![Failed validation](img/failedval.png "Failed validation based upon A5 requirements")
*Screenshot of successful validation:*
![Successful validation](img/successfulval.png "Successful validation based upon A5 requirements")
*Screenshot of updated customer database:*
![Updated database](img/updateddb.png "Updated database based upon A5 requirements")



