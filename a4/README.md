> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 Advanced Web App Development

## Matthew Lewis

### Assignment #4 Requirements:

3 parts:

1. Basic server side validation
2. Provide Bitbucket Read-only access to repo
3. Screenshots: Failed validation, successful validation


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>




#### Assignment Screenshot and Links:)
*Screenshot of failed validation*:
![Failed validation](img/failedval.png "Failed validation based upon A4 requirements")
*Screenshot of successful validation:*
![Successful validation](img/successfulval.png "Successful validation based upon A4 requirements")



