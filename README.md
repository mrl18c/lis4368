> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4368 Advanced Web App Development

## Matthew Lewis

### LIS4368 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Tomcat
    - Proide screenshots of installations
    - Create bitbucket repo
    - Complete bitbucket tutorials
    - Provide git commanad descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Install MySQL Workbench
    - Connect MySQL database with Java servlet
    - Provide screenshots of running various https://localhost:9999 pages
    
3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create ERD
    - Add 10 records to each entity
    - Forward engineer database
    - Provide screenshots of ERD and a3/index.jsp

3. [A4 README.md](a4/README.md "My A4 README.md file")
    - Server side validation
    - Provide bitbucket read-only access
    - Provide screenshots of failed and successful validation

4. [A5 README.md](a5/README.md "My A5 README.md file")
    - Server side form validation
    - Collect customer records and store in mysql database
    - Provide bitbucket read-only access
    - Provide screenshots of failed and successful validation and updated database

5. [P1 README.md](p1/README.md "My P1 README.md file")
    - jQuery form validation
    - Use regular expressions (regexp) for form validation
    - Provide screenshots of course splash page, failed and successful validation

6. [P2 README.md](p2/README.md "My P2 README.md file") 
    - Client and server side form validation
    - Collect, update, and display customer records in MYSQL 
    - Provide screenshots of valid user entry, successful validation, display data, modify form, modified data, delete warning, updated mysql tables
