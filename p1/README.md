> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 Advanced Web App Development

## Matthew Lewis

### Project #1 Requirements:

4 parts:

1. jQuery form validation
2. Use regular expressions (regexp) for form validation
3. Provide Bitbucket Read-only access to repo
4. Screenshots: LIS4368 splash page, Failed validation, Passed validation


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>




#### Assignment Screenshot and Links:
*Screenshot of course splash page*:
![Splash page](img/splashpage.png "Splash page based upon P1 Requirements")
*Screenshot of failed validation*:
![Failed validation](img/failedval.png "Failed validation based upon P1 requirements")
*Screenshot of successful validation:*
![Successful validation](img/successfulval.png "Successful validation based upon P1 requirements")



