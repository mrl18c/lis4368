> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 Advanced Web App Development

## Matthew Lewis

### Assignment #3 Requirements:

3 parts:

1. Entity Relationship Diagram (ERD)
2. Include data (at least 10 record for each table)
3. Provide Bitbucket Read-only access to repo
* docs folder: a3.mwb and a3.sq
* img folder: a3.png
* README.md
4. Screenshot of a3/index.jsp


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>




#### Assignment Screenshot and Links:
*Screenshot A3 ERD*:
![A3 ERD](img/a3.png "ERD based upon A3 Requirements")
*Screenshot of a3/index*:
![A3 Index](img/a3index.png "A3 Index based upon A3 requirements")
*A3 docs: a3.mwb and a3.sql*:
[A3 MWB File](docs/a3.mwb "A3 ERD in .mwb format")
[A3 SQL File](docs/a3.sql "A3 SQL Script")


